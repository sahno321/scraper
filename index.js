require("dotenv").config();
const WaitroseProductScraper = require("./services/WaitroseProductScraper");
const IcelandProductScraper = require("./services/IcelandProductScraper")
const ASDAProductScraper = require("./services/ASDAProductScraper")

async function runScraper() {
  const scraper = new ASDAProductScraper;
  scraper
    .process()
    .then(() => {
      console.log("Finished scraping");
    })
    .catch((err) => {
      console.error("Scraping error",err);
    });
}

runScraper();
