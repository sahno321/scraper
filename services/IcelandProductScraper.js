const Scraper = require("./Scraper");
const puppeteer = require("puppeteer");
const { promises: fs } = require("fs");
const path = require("path");
const axios = require("axios");

const CONFIG_URL = process.env.API_URL + "/scraper/retailerId";
const CATEGORIES_URL = process.env.API_URL + "/scraper-link/retailerId";
const POST_PRODUCT_URL = process.env.API_URL + "/items";
const retailerId = 2;
class IcelandProductScraper extends Scraper {
    constructor() {
        super();
        this.configURL = CONFIG_URL.replace("retailerId", retailerId.toString());
        this.categoriesURL = CATEGORIES_URL.replace(
            "retailerId",
            retailerId.toString()
        );
        this.postURL = POST_PRODUCT_URL;
        this.configSelectors = null;
        this.date = new Date().toISOString().toString();
    }

    async process() {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        const linkCategories = await this.getLinkCategories();
        const configSelectors = await this.getConfigSelectors();

        const dataInfo = [];

        for (const category of linkCategories) {
            await page.goto(category.href);
            console.log(category.href);

            const prodLinks = await page.evaluate((s) => {
                const prodLinks = Array.from(document.querySelectorAll(s.selectorLink));
                return prodLinks.map((li) => li.href);
            }, configSelectors);

            for (const prodLink of prodLinks) {
                await page.goto(prodLink);
                const data = await page.evaluate((selector) => ({
                    name: document.querySelector(selector.name)?.innerText || "",
                    price: document.querySelector(selector.price)?.innerText || "",
                    img: document.querySelector(selector.img)?.href || "",
                    href: document.querySelector(selector.href)?.href || "",
                        description: [...document.querySelectorAll(selector.description)].map(el => el.innerText.replace
                        (/\n/g, " ").trim()).join(" ") || " ",
                }
                ), configSelectors.selector);

                dataInfo.push({
                    ...data,
                    linkId: category.id,
                    date: this.date
                });
                console.log(dataInfo);
            }
        }

        await browser.close();
        await fs.writeFile(path.join("data.json"), JSON.stringify(dataInfo));
        await this.postProducts(dataInfo);

        return dataInfo.map((item) => item.href);
    }

    async getLinkCategories() {
        const response = await axios.get(
            this.categoriesURL,
            await this.getAPIRequest()
        );
        return response.data;
    }

    async postProducts(dataInfo) {
        try {
            await axios.post(this.postURL, dataInfo, await this.getAPIRequest());
            console.log("Successful");
        } catch (err) {
            console.error(err);
        }
    }

    async getConfigSelectors() {
        try {
            if (this.configSelectors) {
                return this.configSelectors;
            }

            const response = await axios.get(
                this.configURL,
                await this.getAPIRequest()
            );
            const configs = response.data;
            const configSelectors = configs.config[0]?.selectors;

            if (
                !configSelectors ||
                !configSelectors.category ||
                !configSelectors.category.links
            ) {
                console.error("Invalid configuration: missing selectorLink");
            }

            this.configSelectors = {
                selectorLink: configSelectors.category.links,
                selector: configSelectors.search,
            };

            return this.configSelectors;
        } catch (error) {
            console.error("Error getting configuration:", error);
            throw error;
        }
    }
}
module.exports = IcelandProductScraper;
