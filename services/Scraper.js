const axios = require("axios");
const AUTH_URL = process.env.API_URL + "/users/login";

class Scraper {
  constructor() {
    this.apiToken = null;
    this.tokenExpiration = null;
  }

  async getAPIToken() {
    try {
      // Check if a valid token exists and has not expired
      if (
        this.apiToken &&
        this.tokenExpiration &&
        this.tokenExpiration > Date.now()
      ) {
        // Return the existing token if it is still valid
        return this.apiToken;
      }

      const payload = {
        username: process.env.API_USERNAME,
        password: process.env.API_PASSWORD,
      };

      const options = {
        headers: { "Content-Type": "application/json" },
      };

      const response = await axios.post(AUTH_URL, payload, options);
      const data = response.data;

      if (!data.token) {
        console.error("Token not received!");
      }

      // Update the token and its expiration time to 3 hours from now
      this.apiToken = data.token;
      this.tokenExpiration = Date.now() + 3 * 60 * 60 * 1000; // 3 hours in milliseconds

      return this.apiToken;
    } catch (e) {
      console.error("Authorization failure", e);
      throw e;
    }
  }

  async getAPIRequest() {
    try {
      const token = await this.getAPIToken();
      return {
        headers: {
          Authorization: `${token}`,
        },
      };
    } catch (error) {
      console.error("API request error", error);
      throw error;
    }
  }
}

module.exports = Scraper;
