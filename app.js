//
// const inquirerPromise = import('inquirer');
// class App{
//     constructor() {
//
//     }
//     async selection() {
//         const inquirer = await inquirerPromise;
//         console.log(`Running on ${process.env.NODE_ENV} mode`);
//         return new Promise((resolve, reject) => {
//             inquirer.default
//                 .prompt([
//                     {
//                         type: 'list',
//                         name: 'retailer',
//                         message: 'Select retailer',
//                         choices: this.ScraperFactory.list,
//                     },
//                 ])
//                 .then(({retailer}) => {
//                     const RetailerFactory = this.ScraperFactory.create(retailer);
//                     inquirer.default
//                         .prompt([
//                             {
//                                 type: 'list',
//                                 name: 'category',
//                                 message: 'Select category',
//                                 choices: RetailerFactory.list,
//                             },
//                         ])
//                         .then(({category}) => {
//                             if (category.includes('products')) {
//                                 inquirer.default
//                                     .prompt([
//                                         {
//                                             type: 'list',
//                                             name: 'type',
//                                             message: 'Select parser type',
//                                             choices: ['full', 'partial'],
//                                         },
//                                     ])
//                                     .then(({type}) => {
//                                         const scraper = RetailerFactory.create(category, {
//                                             isFull: type === 'full',
//                                         });
//                                         scraper
//                                             // .process()
//                                             .then(resolve)
//                                             .catch(reject);
//                                     })
//                                     .catch(e => {
//                                         console.error(e);
//                                         return process.exit(1);
//                                     });
//                             } else {
//                                 const scraper = RetailerFactory.create(category);
//                                 scraper
//                                     .process()
//                                     .then(resolve)
//                                     .catch(reject);
//                             }
//                         })
//                         .catch(reject);
//                 })
//                 .catch(reject);
//         });
//     }
//
//
// }
//
//
// module.exports = new App;
